package angeeeld.utilities;

import angeeeld.model.configuration.ConfigurationBean;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by angeeeld on 23/03/17.
 */
public class Utilities {

    //region CONSTANTS
    private static final String CONFIGURATION_DIRECTORY_PATH = FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath() + "/.ada_project_3";
    private static final String CONFIGURATION_FILE_NAME = "config.json";
    //endregion

    //region CONSTANTS
    //region DATE FORMATS
    public static final String DATE_FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";
    //endregion
    //endregion

    //region Methods
    public static Date parseDate(String dateOnString, String format) {
        Date date = null;
        DateFormat formatter = new SimpleDateFormat(format);

        try {
            date = formatter.parse(dateOnString);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return date;
    }

    public static Integer getYearsBetweenTwoDates(Date initialDate, Date finallyDate) {
        long differenceBetweenDates = Math.abs(finallyDate.getTime() - initialDate.getTime());
        return (int) (TimeUnit.DAYS.convert(differenceBetweenDates, TimeUnit.MILLISECONDS) / 365);
    }

    public static File getConfigurationFile() {
        File configurationDirectory = new File(CONFIGURATION_DIRECTORY_PATH);
        if (!configurationDirectory.exists()) {
            configurationDirectory.mkdirs();
        }
        return new File(configurationDirectory.getAbsolutePath(), CONFIGURATION_FILE_NAME);
    }

    public static ConfigurationBean getConfigurationBean() {
        return new ConfigurationBean(getConfigurationFile());
    }

    public static void createDefaultConfiguration(File configurationFile) {
        ConfigurationBean configurationBean = new ConfigurationBean();
        File databaseFile = new File(FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath()
                + File.separator + ".ada_project_3" + File.separator + "db" + File.separator + "people.txt");
        configurationBean.setmDatabaseFile(databaseFile.getAbsolutePath());
        FileUtils.createEmptyFileIfNotExists(databaseFile);

        Writer fileWriter = FileUtils.createWriterFromFile(configurationFile, false);
        try {
            fileWriter.write(configurationBean.toJsonObject().toString(2));
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String capitalizeString(String stringToCapitalize) {
        return stringToCapitalize.substring(0, 1).toUpperCase() +
                stringToCapitalize.substring(1, stringToCapitalize.length());
    }
    //endregion
}
