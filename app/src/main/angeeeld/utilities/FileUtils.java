package angeeeld.utilities;

import java.io.*;

/**
 * Created by angeeeld on 8/02/17.
 */
public class FileUtils {

    public static Writer createWriterFromFile(File fileToWrite, boolean append) {
        Writer fileWriter = null;
        try {
            fileWriter = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(
                                    fileToWrite, append)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return fileWriter;
    }

    public static void createEmptyFileIfNotExists(File fileToCreate) {
        if (!fileToCreate.exists()) {
            try {
                fileToCreate.getParentFile().mkdirs();
                fileToCreate.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
