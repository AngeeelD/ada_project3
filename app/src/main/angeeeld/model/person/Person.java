package angeeeld.model.person;

import org.json.JSONObject;

/**
 * Created by angeeeld on 23/03/17.
 */
public class Person implements Comparable<Person> {

    //region CONSTANTS
    public static final String PERSON_NAME = "name";
    public static final String PERSON_LAST_NAME = "last_name";
    public static final String PERSON_YEARS = "years";
    public static final String PERSON_GENRE = "genre";
    public static final String PERSON_EMAIL = "email";
    public static final String PERSON_AVATAR_IMAGE_URL = "avatar_image_url";
    //endregion

    //region Global Variables
    private String mName;
    private String mLastName;
    private Integer mYears;
    private String mGenre;
    private String mEmail;
    private String mAvatarImageUrl;
    //endregion

    //region Constructor
    public Person() {
    }

    public Person(String mName, String mLastName, Integer mYears, String mGenre, String mEmail, String mAvatarImageUrl) {
        this.mName = mName;
        this.mLastName = mLastName;
        this.mYears = mYears;
        this.mGenre = mGenre;
        this.mEmail = mEmail;
        this.mAvatarImageUrl = mAvatarImageUrl;
    }

    public Person(JSONObject personInformation) {
        if (personInformation.has(PERSON_NAME) && !personInformation.isNull(PERSON_NAME)) {
            this.mName = personInformation.getString(PERSON_NAME);
        }
        if (personInformation.has(PERSON_LAST_NAME) && !personInformation.isNull(PERSON_LAST_NAME)) {
            this.mLastName = personInformation.getString(PERSON_LAST_NAME);
        }
        if (personInformation.has(PERSON_YEARS) && !personInformation.isNull(PERSON_YEARS)) {
            this.mYears = personInformation.getInt(PERSON_YEARS);
        }
        if (personInformation.has(PERSON_GENRE) && !personInformation.isNull(PERSON_GENRE)) {
            this.mGenre = personInformation.getString(PERSON_GENRE);
        }
        if (personInformation.has(PERSON_EMAIL) && !personInformation.isNull(PERSON_EMAIL)) {
            this.mEmail = personInformation.getString(PERSON_EMAIL);
        }
        if (personInformation.has(PERSON_AVATAR_IMAGE_URL) && !personInformation.isNull(PERSON_AVATAR_IMAGE_URL)) {
            this.mAvatarImageUrl = personInformation.getString(PERSON_AVATAR_IMAGE_URL);
        }
    }
    //endregion

    //region Getters & Setter
    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public Integer getmYears() {
        return mYears;
    }

    public void setmYears(Integer mYears) {
        this.mYears = mYears;
    }

    public String getmGenre() {
        return mGenre;
    }

    public void setmGenre(String mGenre) {
        this.mGenre = mGenre;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmAvatarImageUrl() {
        return mAvatarImageUrl;
    }

    public void setmAvatarImageUrl(String mAvatarImageUrl) {
        this.mAvatarImageUrl = mAvatarImageUrl;
    }

    //endregion

    //region Object methods


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;

        if (!getmName().equals(person.getmName())) return false;
        if (!getmLastName().equals(person.getmLastName())) return false;
        if (!getmYears().equals(person.getmYears())) return false;
        if (!getmGenre().equals(person.getmGenre())) return false;
        if (!getmEmail().equals(person.getmEmail())) return false;
        return getmAvatarImageUrl() != null ? getmAvatarImageUrl().equals(person.getmAvatarImageUrl()) : person.getmAvatarImageUrl() == null;
    }

    @Override
    public int hashCode() {
        int result = getmName().hashCode();
        result = 31 * result + getmLastName().hashCode();
        result = 31 * result + getmYears().hashCode();
        result = 31 * result + getmGenre().hashCode();
        result = 31 * result + getmEmail().hashCode();
        result = 31 * result + (getmAvatarImageUrl() != null ? getmAvatarImageUrl().hashCode() : 0);
        return Math.abs(result) % 4;
    }

    @Override
    public String toString() {
        return this.getmName() + " " + this.getmLastName() + ", " + this.getmYears() + " years old";
    }

    //endregion

    //region Conversion Methods
    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(PERSON_NAME, getmName());
        jsonObject.put(PERSON_LAST_NAME, getmLastName());
        jsonObject.put(PERSON_YEARS, getmYears());
        jsonObject.put(PERSON_GENRE, getmGenre());
        jsonObject.put(PERSON_EMAIL, getmEmail());
        if (getmAvatarImageUrl() != null) {
            jsonObject.put(PERSON_AVATAR_IMAGE_URL, getmAvatarImageUrl());
        }
        return jsonObject;
    }

    @Override
    public int compareTo(Person otherPerson) {
        return this.getmYears() < otherPerson.getmYears() ? -1 : 1;
    }
    //endregion

}
