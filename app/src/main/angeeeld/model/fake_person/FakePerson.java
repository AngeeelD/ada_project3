
package angeeeld.model.fake_person;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FakePerson {

    private List<Result> results = null;
    private Info info;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public FakePerson() {
    }

    /**
     * @param results
     * @param info
     */
    public FakePerson(List<Result> results, Info info) {
        super();
        this.results = results;
        this.info = info;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public FakePerson withResults(List<Result> results) {
        this.results = results;
        return this;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public FakePerson withInfo(Info info) {
        this.info = info;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FakePerson withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
