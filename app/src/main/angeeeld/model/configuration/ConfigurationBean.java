package angeeeld.model.configuration;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by angeeeld on 12/02/17.
 */
public class ConfigurationBean {

    //region CONSTANTS
    private static final String FILE_TO_CREATE_DIRECTORY_PATH_KEY = "directoryForCreatedDatabase";
    //endregion

    //region Global Variables
    private String mDatabaseFile;
    //endregion

    //region Constructor

    public ConfigurationBean(File configurationFile) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(configurationFile));
            String line = "";
            String configurationInformation = "";
            while ((line = bufferedReader.readLine()) != null) {
                configurationInformation += line;
            }
            JSONObject jsonObject = new JSONObject(configurationInformation);
            if (jsonObject.has(FILE_TO_CREATE_DIRECTORY_PATH_KEY)) {
                this.mDatabaseFile = jsonObject.getString(FILE_TO_CREATE_DIRECTORY_PATH_KEY);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ConfigurationBean(String mDatabaseFile) {
        this.mDatabaseFile = mDatabaseFile;
    }

    public ConfigurationBean() {

    }

    //endregion

    //region Local Methods

    //endregion

    //region Getters & Setters

    public String getmDatabaseFile() {
        return mDatabaseFile;
    }

    public void setmDatabaseFile(String mDatabaseFile) {
        this.mDatabaseFile = mDatabaseFile;
    }

    //endregion

    //region Conversion Methods
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(FILE_TO_CREATE_DIRECTORY_PATH_KEY, getmDatabaseFile());
        return jsonObject;
    }
    //endregion

}
