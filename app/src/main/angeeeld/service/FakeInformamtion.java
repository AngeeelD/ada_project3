package angeeeld.service;

import angeeeld.model.fake_person.FakePerson;
import com.sun.istack.internal.Nullable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by angeeeld on 10/03/17.
 */
public interface FakeInformamtion {

    @GET("/")
    Observable<FakePerson> getPeople(@Nullable @Query("nat") String nationality,
                                     @Nullable @Query("seed") String seed,
                                     @Nullable @Query("results") Integer amount,
                                     @Nullable @Query("gender") String gender);

}
