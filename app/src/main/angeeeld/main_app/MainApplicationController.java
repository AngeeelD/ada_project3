package angeeeld.main_app;

import angeeeld.data_structure.RedBlackTree;
import angeeeld.model.person.Person;
import angeeeld.utilities.Utilities;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.Pair;
import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;

import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * Created by angeeeld on 10/03/17.
 */
public class MainApplicationController implements Initializable, MainApplicationContract.View {

    //region Global Variables
    //region Binding
    //region Main App
    @FXML
    TabPane mMainTabPane;
    @FXML
    Tab mPeopleListTab;
    @FXML
    Tab mAddPersonTab;
    //endregion
    //region People Tab
    @FXML
    ListView<Person> mPeopleList;
    @FXML
    Button mAddPersonActionFromList;
    @FXML
    Button mDeletePersonAction;
    @FXML
    Button mDeletePeopleAction;
    //endregion
    //region Add Person Tab
    @FXML
    TextField mPersonName;
    @FXML
    TextField mPersonLastName;
    @FXML
    TextField mPersonYears;
    @FXML
    TextField mPersonEmail;
    @FXML
    Label mErrorMessage1;
    @FXML
    Label mErrorMessage2;
    @FXML
    RadioButton mPersonGenreMale;
    @FXML
    RadioButton mPersonGenreFemale;
    @FXML
    ImageView mCreateRandomPerson;
    @FXML
    ImageView mPersonAvatar;
    @FXML
    Button mPersonAddAction;
    //endregion
    //region People Structure
    @FXML
    TreeView<String> mPeopleStructureTree;
    //endregion
    //endregion

    private MainApplicationContract.Presenter mPresenter;
    private WritableImage mRoundRectImage;

    private Person mCurrentPerson;
    //endregion

    //region Events
    //region People List Tab
    @FXML
    public void addPersonFromListAction(ActionEvent event) {
        SingleSelectionModel<Tab> selectionModel = mMainTabPane.getSelectionModel();
        selectionModel.select(mAddPersonTab);
        mPresenter.setCallbackToList(true);
    }

    @FXML
    public void deletePerson(ActionEvent event) {
        mPresenter.deletePerson(mPeopleList.getSelectionModel().getSelectedItem());
    }

    @FXML
    void deletePeople(ActionEvent event) {
        mPresenter.cleanDatabaseFile();
    }

    @FXML
    void onAddPersonTabSelected() {
        mPresenter.setCallbackToList(false);
    }

    @FXML
    void mPeopleListTabSelected() {
        if (mPresenter != null) {
            mPresenter.readDatabaseForPopulatePeopleList();
        }
    }

    //endregion
    //region Add Person Tab
    @FXML
    public void onMaleSelected(ActionEvent event) {
        mPersonGenreFemale.setSelected(false);
        Platform.runLater(this::showAddButton);
    }

    @FXML
    public void onFemaleSelected(ActionEvent event) {
        mPersonGenreMale.setSelected(false);
        Platform.runLater(this::showAddButton);
    }

    @FXML
    public void onNameTextChanged(KeyEvent inputMethodEvent) {
        Platform.runLater(this::showAddButton);
    }

    @FXML
    public void onLastNameTextChanged(KeyEvent keyEvent) {
        Platform.runLater(this::showAddButton);
    }

    @FXML
    public void onYearsTextChanged(KeyEvent keyEvent) {
        Platform.runLater(this::showAddButton);
    }

    @FXML
    public void onEmailTextChanged(KeyEvent keyEvent) {
        Platform.runLater(this::showAddButton);
    }

    @FXML
    public void onGetFakePersonInformationPressed(MouseEvent mouseEvent) {
        setEnableAllViews(false);
        Platform.runLater(() -> mPresenter.getFakePerson());
    }

    @FXML
    public void onAddPersonPressed(ActionEvent event) {
        mPresenter.createPerson(mCurrentPerson);
        mPresenter.readDatabaseForPopulatePeopleList();
    }
    //endregion
    //endregion

    //region Initializable
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mPresenter = new MainApplicationPresenter(this);
        mPresenter.readDatabaseForPopulatePeopleList();

        if (!Utilities.getConfigurationFile().exists()) {
            Utilities.createDefaultConfiguration(Utilities.getConfigurationFile());
        }

        mPersonAddAction.setDisable(true);

        //region Avatar Round Rect
        Rectangle clip = new Rectangle(mPersonAvatar.getFitWidth(), mPersonAvatar.getFitHeight());
        clip.setArcWidth(20);
        clip.setArcHeight(20);
        mPersonAvatar.setClip(clip);

        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        mRoundRectImage = mPersonAvatar.snapshot(parameters, null);
        mPersonAvatar.setEffect(new DropShadow(20, Color.BLACK));
        //endregion

        mPeopleList.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                            if (newValue != null && !newValue.equals("")) {
                                mDeletePersonAction.setDisable(false);
                            }
                        }
                );
    }
    //endregion

    //region Local methods
    private void cleanFields() {
        mPersonName.setText("");
        mPersonLastName.setText("");
        mPersonYears.setText("");
        mPersonEmail.setText("");
        mPersonAvatar.setImage(null);
        mPersonGenreMale.setSelected(false);
        mPersonGenreFemale.setSelected(false);
        mPersonAddAction.setDisable(true);
    }

    private void showAddButton() {
        if (allFieldsAreOk()) {
            updateCurrentPerson();
            mPersonAddAction.setDisable(false);
        } else {
            mCurrentPerson = null;
            mPersonAddAction.setDisable(true);
        }
    }

    private boolean allFieldsAreOk() {
        try {
            return !mPersonName.getText().isEmpty()
                    && !mPersonLastName.getText().isEmpty()
                    && !mPersonYears.getText().isEmpty()
                    && !mPersonEmail.getText().isEmpty()
                    && Integer.parseInt(mPersonYears.getText()) > 0
                    && (mPersonGenreMale.isSelected() || mPersonGenreFemale.isSelected());
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void changeToPeopleListIfIsNecessary() {
        if (mPresenter.getCallbackToList()) {
            SingleSelectionModel<Tab> selectionModel = mMainTabPane.getSelectionModel();
            selectionModel.select(mPeopleListTab);
        }
    }

    private void updateCurrentPerson() {
        if (mCurrentPerson == null) {
            mCurrentPerson = new Person();
        }
        mCurrentPerson.setmName(mPersonName.getText());
        mCurrentPerson.setmLastName(mPersonLastName.getText());
        mCurrentPerson.setmYears(Integer.parseInt(mPersonYears.getText()));
        mCurrentPerson.setmEmail(mPersonEmail.getText());
        mCurrentPerson.setmGenre(mPersonGenreMale.isSelected() ? "male" : mPersonGenreFemale.isSelected() ? "female" : "");
    }
    //endregion

    //region MainApplicationContract [View]
    @Override
    public void onFakePersonWasObtained(Person person) {
        this.mCurrentPerson = person;
        Platform.runLater(() -> {
            try {
                mPersonName.setText(person.getmName());
                mPersonLastName.setText(person.getmLastName());
                mPersonYears.setText("" + person.getmYears());
                mPersonGenreMale.setSelected((person.getmGenre().equals("male")));
                mPersonGenreFemale.setSelected((person.getmGenre().equals("female")));
                mPersonEmail.setText(person.getmEmail());
                Image avatarImage = new Image(person.getmAvatarImageUrl());

                mPersonAvatar.setImage(avatarImage);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void setEnableAllViews(Boolean enableAllViews) {
        Platform.runLater(() -> {
            mPersonName.setDisable(!enableAllViews);
            mPersonLastName.setDisable(!enableAllViews);
            mPersonYears.setDisable(!enableAllViews);
            mPersonGenreMale.setDisable(!enableAllViews);
            mPersonGenreFemale.setDisable(!enableAllViews);
            mPersonEmail.setDisable(!enableAllViews);
            mCreateRandomPerson.setDisable(!enableAllViews);
            mPersonAddAction.setDisable(!enableAllViews);
            if (!enableAllViews) {
                mPersonAvatar.setImage(null);
                mCreateRandomPerson.setImage(new Image("ic_load_fake_person_disable.png"));
            } else {
                mCreateRandomPerson.setImage(new Image("ic_load_fake_person.png"));
            }
            this.showAddButton();
        });
    }

    private Subscription appearEffect;
    private Subscription disappearEffect;

    @Override
    public void onGetFakePersonFailed() {
        if (appearEffect != null && disappearEffect != null && (!appearEffect.isUnsubscribed() || !disappearEffect.isUnsubscribed()))
            return;
        appearEffect = Observable.zip(
                Observable.range(0, 100),
                Observable.interval(15, TimeUnit.MILLISECONDS),
                (integer, aLong) -> integer.doubleValue() / 100)
                .subscribeOn(Schedulers.newThread())
                .subscribe(alphaValue ->
                        Platform.runLater(() -> {
                            mErrorMessage1.setOpacity(alphaValue);
                            mErrorMessage2.setOpacity(alphaValue);
                        })
                );

        disappearEffect = Observable.zip(
                Observable.range(0, 100)
                        .toList()
                        .map(integers -> {
                            Collections.reverse(integers);
                            return integers;
                        })
                        .flatMap(Observable::from),
                Observable.interval(4000, 15, TimeUnit.MILLISECONDS),
                (integer, aLong) -> integer.doubleValue() / 100)
                .subscribeOn(Schedulers.newThread())
                .subscribe(alphaValue_2 -> {
                    System.out.println("" + alphaValue_2);
                    Platform.runLater(() -> {
                        mErrorMessage1.setOpacity(alphaValue_2);
                        mErrorMessage2.setOpacity(alphaValue_2);
                    });
                });
    }

    @Override
    public void populatePeopleList(ObservableList<Person> peopleList) {
        if (peopleList.size() > 0) {
            mDeletePeopleAction.setDisable(false);
        } else {
            mDeletePeopleAction.setDisable(true);
        }
        mPeopleList.setItems(peopleList);
        mPeopleList.setCellFactory(new Callback<ListView<Person>, ListCell<Person>>() {
            @Override
            public ListCell<Person> call(ListView<Person> param) {
                return new ListCell<Person>() {
                    @Override
                    protected void updateItem(Person item, boolean empty) {
                        Platform.runLater(() -> {
                            super.updateItem(item, empty);
                            if (item != null) {
                                Text nameText = new Text(Utilities.capitalizeString(item.getmName())
                                        + " " + Utilities.capitalizeString(item.getmLastName()));
                                nameText.setFont(Font.font(null, FontWeight.BOLD, 14));

                                Text age = new Text(item.getmYears() + " Years old");
                                age.setFont(Font.font(null, FontWeight.LIGHT, 12));

                                Text email = new Text(item.getmEmail());
                                email.setFont(Font.font(null, FontWeight.EXTRA_BOLD, 12));

                                VBox vBox = new VBox(nameText, email, age);
                                ImageView avatarImage = new ImageView();
                                if (item.getmAvatarImageUrl() != null) {
                                    avatarImage = new ImageView(item.getmAvatarImageUrl());
                                }
                                avatarImage.setFitWidth(48);
                                avatarImage.setFitHeight(48);
                                HBox hBox = new HBox(avatarImage, vBox);
                                hBox.setSpacing(10);
                                setGraphic(hBox);
                            }
                        });
                    }
                };
            }
        });
    }

    @Override
    public void onPersonWasCreated() {
        cleanFields();
        changeToPeopleListIfIsNecessary();
    }

    @Override
    public void onPeopleListWasCleaned() {
        changeToPeopleListIfIsNecessary();
        mPresenter.readDatabaseForPopulatePeopleList();
    }

    @Override
    public void updatePeopleStructureTree(HashMap<Integer, RedBlackTree<Person>> peopleStructure) {
        Platform.runLater(() -> {
            TreeItem<String> rootItem = new TreeItem<>("people structure", new ImageView("ic_contacts.png"));
            for (Integer key : peopleStructure.keySet()) {
                Pair<TreeItem<String>, Integer> peopleTree = mPresenter.getExpandedTreeItem(peopleStructure.get(key).getRoot());
                TreeItem<String> arrayItem = new TreeItem<>("" + key + "(" + (peopleTree.getValue() + 1) + ")", new ImageView("ic_group.png"));

                rootItem.getChildren().add(arrayItem);
                arrayItem.getChildren().add(peopleTree.getKey());
            }
            rootItem.setExpanded(rootItem.getChildren().size() > 0);
            mPeopleStructureTree.setRoot(rootItem);
        });
    }
    //endregion

}
