package angeeeld.main_app;

import angeeeld.data_structure.RedBlackNode;
import angeeeld.data_structure.RedBlackTree;
import angeeeld.model.person.Person;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.util.Pair;

import java.util.HashMap;

/**
 * Created by angeeeld on 23/03/17.
 */
public interface MainApplicationContract {

    interface View {

        void onFakePersonWasObtained(Person person);

        void setEnableAllViews(Boolean enableAllViews);

        void onGetFakePersonFailed();

        void populatePeopleList(ObservableList<Person> peopleList);

        void onPersonWasCreated();

        void onPeopleListWasCleaned();

        void updatePeopleStructureTree(HashMap<Integer, RedBlackTree<Person>> peopleStructure);

    }

    interface Presenter {

        void getFakePerson();

        void readDatabaseForPopulatePeopleList();

        void setCallbackToList(boolean callbackToList);

        boolean getCallbackToList();

        void createPerson(Person person);

        void cleanDatabaseFile();

        void deletePerson(Person person);

        Pair<TreeItem<String>, Integer> getExpandedTreeItem(RedBlackNode<Person> root);

    }


}
