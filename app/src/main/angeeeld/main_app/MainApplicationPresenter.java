package angeeeld.main_app;

import angeeeld.data_structure.RedBlackNode;
import angeeeld.data_structure.RedBlackTree;
import angeeeld.model.fake_person.FakePerson;
import angeeeld.model.person.Person;
import angeeeld.service.FakeInformamtion;
import angeeeld.service.core.ServiceRestGenerator;
import angeeeld.utilities.FileUtils;
import angeeeld.utilities.Utilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.util.Pair;
import org.json.JSONObject;
import rx.Observable;
import rx.observables.StringObservable;
import rx.schedulers.Schedulers;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by angeeeld on 23/03/17.
 */
public class MainApplicationPresenter implements MainApplicationContract.Presenter {

    //region Global Variables
    private MainApplicationContract.View mView;

    private boolean mCallbackToPeopleList;

    private HashMap<Integer, RedBlackTree<Person>> mPeopleTree;
    //endregion

    //region Constructor
    public MainApplicationPresenter(MainApplicationContract.View mView) {
        this.mView = mView;
        mPeopleTree = new HashMap<>();
    }
    //endregion

    //region MainApplicationContract [PRESENTER]
    @Override
    public void getFakePerson() {
        ServiceRestGenerator.createService(FakeInformamtion.class)
                .getPeople(null, null, null, null)
                .map(FakePerson::getResults)
                .flatMap(Observable::from)
                .subscribe(fakePerson -> mView.onFakePersonWasObtained(
                        new Person(fakePerson.getName().getFirst(),
                                fakePerson.getName().getLast(),
                                Utilities.getYearsBetweenTwoDates(
                                        Utilities.parseDate(fakePerson.getDob(), Utilities.DATE_FORMAT_LONG), new Date()),
                                fakePerson.getGender(),
                                fakePerson.getEmail(),
                                fakePerson.getPicture().getLarge())),
                        throwable -> {
                            throwable.printStackTrace();
                            mView.onGetFakePersonFailed();
                            mView.setEnableAllViews(true);
                        },
                        () -> mView.setEnableAllViews(true));
    }

    @Override
    public void readDatabaseForPopulatePeopleList() {
        Observable.just(1)
                .observeOn(Schedulers.newThread())
                .map(integer -> getPeopleFromDatabase())
                .subscribe(this::readDatabaseForPopulatePeopleList,
                        Throwable::printStackTrace);
    }

    @Override
    public void setCallbackToList(boolean callbackToList) {
        this.mCallbackToPeopleList = callbackToList;
    }

    @Override
    public boolean getCallbackToList() {
        return mCallbackToPeopleList;
    }

    @Override
    public void createPerson(Person person) {
        Observable.just(person)
                .observeOn(Schedulers.newThread())
                .subscribe(personToWrite -> {
                            Writer personWriter =
                                    FileUtils.createWriterFromFile(
                                            new File(Utilities.getConfigurationBean().getmDatabaseFile()), true);
                            try {
                                personWriter.write(personToWrite.toJSONObject().toString() + "\n");
                                personWriter.flush();
                                personWriter.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        },
                        Throwable::printStackTrace,
                        () -> mView.onPersonWasCreated());
    }

    @Override
    public void cleanDatabaseFile() {
        Observable.just(1)
                .observeOn(Schedulers.newThread())
                .subscribe(integer -> {
                            Writer personWriter =
                                    FileUtils.createWriterFromFile(
                                            new File(Utilities.getConfigurationBean().getmDatabaseFile()), false);
                            try {
                                personWriter.write("");
                                personWriter.flush();
                                personWriter.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        },
                        Throwable::printStackTrace,
                        () -> {
                            mView.onPeopleListWasCleaned();
                            readDatabaseForPopulatePeopleList();
                        });
    }

    @Override
    public void deletePerson(Person person) {
        List<Person> currentPeople = getPeopleFromDatabase();
        currentPeople.remove(currentPeople.indexOf(person));
        cleanDatabaseFile();
        for (Person personToWrite : currentPeople) {
            createPerson(personToWrite);
        }
        readDatabaseForPopulatePeopleList(currentPeople);
    }

    @Override
    public Pair<TreeItem<String>, Integer> getExpandedTreeItem(RedBlackNode<Person> root) {
        if (root.key == null) return null;
        ImageView avatarImage = new ImageView(root.getKey().getmAvatarImageUrl());
        avatarImage.setFitWidth(24);
        avatarImage.setFitHeight(24);

        int count = 0;

        List<TreeItem<String>> leafs = new ArrayList<>();

        if (root.getLeft().getKey() != null) {
            Pair<TreeItem<String>, Integer> leftPerson = getExpandedTreeItem(root.getLeft());
            leafs.add(leftPerson.getKey());
            count += leftPerson.getValue() + 1;
        }

        if (root.getRight().getKey() != null) {
            Pair<TreeItem<String>, Integer> rightPerson = getExpandedTreeItem(root.getRight());
            leafs.add(rightPerson.getKey());
            count += rightPerson.getValue() + 1;
        }

        TreeItem<String> personItemTree = new TreeItem<>(root.getKey().toString() + "(" + count + ")", avatarImage);

        for (TreeItem<String> leaf : leafs) {
            personItemTree.getChildren().add(leaf);
        }

        personItemTree.setExpanded(true);

        Pair<TreeItem<String>, Integer> personItemPair = new Pair<>(personItemTree, count);

        return personItemPair;
    }
    //endregion

    //region Local Methods
    private List<Person> getPeopleFromDatabase() {
        try {
            return StringObservable.split(
                    StringObservable.from(
                            new BufferedReader(
                                    new FileReader(
                                            new File(Utilities.getConfigurationBean().getmDatabaseFile())))),
                    "\n")
                    .map(JSONObject::new)
                    .map(Person::new)
                    .toList()
                    .toBlocking()
                    .first();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void readDatabaseForPopulatePeopleList(List<Person> people) {
        ObservableList<Person> peopleToPopulate = FXCollections.observableArrayList();
        Observable.just(people)
                .subscribe(peopleList -> {
                            Person[] peopleArray = new Person[peopleList.size()];
                            peopleList.toArray(peopleArray);
                            peopleToPopulate.addAll(peopleArray);
                            updatePeopleTree(peopleList);
                        },
                        Throwable::printStackTrace,
                        () -> mView.populatePeopleList(peopleToPopulate));
    }

    private void updatePeopleTree(List<Person> people) {
        mPeopleTree = new HashMap<>();
        for (Person person : people) {
            if (!mPeopleTree.containsKey(Integer.valueOf(person.hashCode()))) {
                RedBlackTree<Person> peopleTree = new RedBlackTree<>();
                peopleTree.insert(person);
                mPeopleTree.put(Integer.valueOf(person.hashCode()), peopleTree);
            } else {
                RedBlackTree<Person> peopleTree = mPeopleTree.get(Integer.valueOf(person.hashCode()));
                peopleTree.insert(person);
                mPeopleTree.put(Integer.valueOf(person.hashCode()), peopleTree);
            }
        }
        mView.updatePeopleStructureTree(mPeopleTree);
    }
    //endregion

}
